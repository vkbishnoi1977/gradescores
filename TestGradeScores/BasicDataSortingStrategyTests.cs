﻿using FluentAssertions;
using grade_scores.BusinessLogic;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TestGradeScores
{
    [TestFixture]
    public sealed class BasicDataSortingStrategyTests
    {
        BasicDataSortingStrategy _dataSortStrategy;

        [OneTimeSetUp]
        public void Setup()
        {
            _dataSortStrategy = new BasicDataSortingStrategy();

        }

        [Test]
        public void ShouldThrowExceptionWithNullDataset()
        {
            //Act
            Action act = () => _dataSortStrategy.SortData(null);

            //Assert
            act.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain("There is no data to sort.");
        }

        [Test]
        public void ShouldThrowExceptionWithEmptyDataset()
        {
            //Act
            Action act = () => _dataSortStrategy.SortData(new List<GradeData>());

            //Assert
            act.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain("There is no data to sort.");
        }

        [Test]
        public void ShouldSortData()
        {
            //Arrange
            var data = new List<GradeData>();
            var dataObj1 = new GradeData
            {
                Grade = 80,
                FirstName = "Alpha",
                LastName = "Beta",
            };

            var dataObj2 = new GradeData
            {
                Grade = 85,
                FirstName = "Alpha",
                LastName = "Zulu",
            };

            var dataObj3 = new GradeData
            {
                Grade = 75,
                FirstName = "Alpha",
                LastName = "Zulu",
            };

            data.Add(dataObj1);
            data.Add(dataObj2);
            data.Add(dataObj3);

            //Act
            var sortedData = _dataSortStrategy.SortData(data).ToList();

            //Assert
            sortedData.Should().NotBeNullOrEmpty();
            sortedData.Count.Should().Be(3);
            sortedData[0].Should().Be(dataObj2);
            sortedData[1].Should().Be(dataObj1);
            sortedData[2].Should().Be(dataObj3);
        }
    }
}
