﻿using FluentAssertions;
using grade_scores.BusinessLogic;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;

namespace TestGradeScores
{
    [TestFixture]
    public sealed class BasicFileHandlingStrategyTests
    {
        BasicFileHandlingStrategy _fileHandlingStrategy;

        [OneTimeSetUp]
        public void Setup()
        {
            _fileHandlingStrategy = new BasicFileHandlingStrategy();
        }

        [Test]
        public void ShouldThrowExceptionWithInvalidFileName()
        {
            //Act
            Action act = () => _fileHandlingStrategy.ParseFile(@"d:\fakedirectory\gradeinput.txt");

            //Assert
            act.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain("File d:\\fakedirectory\\gradeinput.txt does not exist.");

        }

        [Test]
        [TestCase("ABC, XYZ")]
        [TestCase("ABC, XYZ, XX")]
        public void ShouldNotParseFileWithWrongFormatData(string data)
        {
            //Arrange
            var filePath = $"{AppDomain.CurrentDomain.BaseDirectory}\\InvalidInput.txt";
            using (var streamWriter = new StreamWriter(filePath))
            {
                streamWriter.WriteLine(data);
                streamWriter.Close();
            }

            //Act
            Action act = () => _fileHandlingStrategy.ParseFile(filePath);

            //Assert
            act.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain("Invalid file data.");

            //Cleanup
            File.Delete(filePath);

        }

        [Test]
        public void ShouldParseFile()
        {
            //Arrange
            var filePath = $"{AppDomain.CurrentDomain.BaseDirectory}TestData\\Input.txt";

            //Act
            var parsedData = _fileHandlingStrategy.ParseFile(filePath);

            //Assert
            parsedData.Should().NotBeNullOrEmpty();
            parsedData.Count.Should().Be(4);
            parsedData[0].ToString().ShouldBeEquivalentTo("BUNDY, TERESSA, 88");
            parsedData[1].ToString().ShouldBeEquivalentTo("SMITH, ALLAN, 70");
            parsedData[2].ToString().ShouldBeEquivalentTo("KING, MADISON, 88");
            parsedData[3].ToString().ShouldBeEquivalentTo("SMITH, FRANCIS, 85");

        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        [TestCase("http:\\abcd.txt")]
        [TestCase(@"xyz:\abcd.txt\input.txt")]
        [TestCase(@"c:\abcd.txt\input.txt")]
        public void ShouldNotCreateNewFileWithInvalidFileName(string fileName)
        {
            //Arrange
            var data = new List<GradeData>();
            data.Add(new GradeData
            {
                FirstName = "Alpha",
                LastName = "Beta",
                Grade = 1
            });

            //Act
            Action act = () => _fileHandlingStrategy.WriteFile(fileName, data);

            //Assert
            act.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain("Invalid file path.");
        }

        [Test]
        public void ShouldCreateNewFile()
        {
            //Arrange
            var data = new List<GradeData>();
            data.Add(new GradeData
            {
                FirstName = "Alpha",
                LastName = "Beta",
                Grade = 1
            });

            var filePath = $"{AppDomain.CurrentDomain.BaseDirectory}Input-Graded.txt";

            //Act
            _fileHandlingStrategy.WriteFile(filePath, data);

            //Assert
            File.Exists(filePath).Should().BeTrue();

            //Cleanup
            File.Delete(filePath);
        }
    }
}
