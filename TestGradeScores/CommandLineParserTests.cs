﻿using FluentAssertions;
using grade_scores.BusinessLogic;
using NUnit.Framework;
using System;

namespace TestGradeScores
{
    [TestFixture]
    public class CommandLineParserTests
    {
        [Test]
        public void ShouldThrowExceptionWithNullParameter()
        {
            //Arrange
            var cmdLineParser = new CommandLineParser();

            //Act
            Action act = () => cmdLineParser.ParseFileName(null);

            //Assert
            act.ShouldThrow<ArgumentNullException>()
                .And.Message.Should().Contain("Input file not specified."); 
        }

        [Test]
        public void ShouldThrowExceptionWithMoreThanOneCommandlineArguments()
        {
            //Arrange
            var cmdLineParser = new CommandLineParser();

            //Act
            Action act = () => cmdLineParser.ParseFileName(new string[] { @"d:\fakedirectory\gradeinput.txt", "Hi there" });

            //Assert
            act.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain("More than one command line arguments provided.");
        }

        [Test]
        public void ShouldThrowExceptionWithInvalidFileName()
        {
            //Arrange
            var cmdLineParser = new CommandLineParser();

            //Act
            Action act = () => cmdLineParser.ParseFileName(new string[] { @"d:\fakedirectory\gradeinput.txt" });

            //Assert
            act.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain($"File d:\\fakedirectory\\gradeinput.txt does not exist.");

        }

        [Test]
        public void ShouldReturnFileName()
        {
            //Arrange
            var cmdLineParser = new CommandLineParser();
            var filePath = $"{AppDomain.CurrentDomain.BaseDirectory}TestData\\Input.txt";            


            //Act
            var parsedFileName = cmdLineParser.ParseFileName(new string[] { filePath });

            //Assert
            parsedFileName.ShouldBeEquivalentTo(filePath);            

        }
    }
}
