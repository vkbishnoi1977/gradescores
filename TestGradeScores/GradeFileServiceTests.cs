﻿using FluentAssertions;
using grade_scores.BusinessLogic;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;

namespace TestGradeScores
{
    [TestFixture]
    public class GradeFileServiceTests
    {
        GradeFileService _gradeFileService;
        [OneTimeSetUp]
        public void Setup()
        {
            _gradeFileService = new GradeFileService(new BasicFileHandlingStrategy(),
                new BasicDataSortingStrategy());
        }


        [Test]
        public void ShouldNotCreateInstanceWithNullFileHandlingStrategy()
        {
            Action act = () => new GradeFileService(null, new Mock<IDataSortingStrategy>().Object);
            act.ShouldThrow<ArgumentNullException>().And.Message.Should().Contain("Null argument passed: fileParsingStrategy");
        }

        [Test]
        public void ShouldNotCreateInstanceWithNullDataSortingStrategy()
        {
            Action act = () => new GradeFileService(new Mock<IFileHandlingStrategy>().Object, null);
            act.ShouldThrow<ArgumentNullException>().And.Message.Should().Contain("Null argument passed: dataSortingStrategy");
        }

        [Test]
        public void ShouldThrowExceptionWithNullParameter()
        {
            //Act
            Action act = () => _gradeFileService.ParseFile(null);

            //Assert
            act.ShouldThrow<ArgumentNullException>()
                .And.Message.Should().Contain("No file name specified.");
        }

        [Test]
        public void ShouldThrowExceptionWithInvalidFileName()
        {
            //Act
            Action act = () => _gradeFileService.ParseFile(@"d:\fakedirectory\gradeinput.txt");

            //Assert
            act.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain("File d:\\fakedirectory\\gradeinput.txt does not exist.");

        }
        

        [Test]
        public void ShouldNotCreateNewFileWithNoData()
        {
            //Act
            Action act = () => _gradeFileService.CreateNewFileWithSortedData(null, "output.txt");

            //Assert
            act.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain("There is no data to sort.");
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        [TestCase("http:\\abcd.txt")]
        [TestCase(@"xyz:\abcd.txt\input.txt")]
        [TestCase(@"c:\abcd.txt\input.txt")]
        public void ShouldNotCreateNewFileWithInvalidFileName(string fileName)
        {
            //Arrange
            var data = new List<GradeData>();
            data.Add(new GradeData
            {
                FirstName = "Alpha",
                LastName = "Beta",
                Grade = 1
            });

            //Act
            Action act = () => _gradeFileService.CreateNewFileWithSortedData(data, fileName);

            //Assert
            act.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain("Invalid file path.");
        }

        [Test]
        public void ShouldCreateNewFile()
        {
            //Arrange
            var data = new List<GradeData>();
            data.Add(new GradeData
            {
                FirstName = "Alpha",
                LastName = "Beta",
                Grade = 1
            });

            var filePath = $"{AppDomain.CurrentDomain.BaseDirectory}Input-Graded.txt";

            //Act
            _gradeFileService.CreateNewFileWithSortedData(data, filePath);

            //Assert
            File.Exists(filePath).Should().BeTrue();

            //Cleanup
            File.Delete(filePath);
        }
    }
}
