﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace grade_scores.BusinessLogic
{
    public sealed class BasicDataSortingStrategy : IDataSortingStrategy
    {
        public IEnumerable<GradeData> SortData(IEnumerable<GradeData> parsedData)
        {
            if (parsedData == null || parsedData.Count() == 0)
            {
                throw new ArgumentException("There is no data to sort.");
            }

            //First by grade descending and then by lastname asc and then by first name asc
            return parsedData.OrderByDescending(a => a.Grade)
                                .ThenBy(a => a.LastName, StringComparer.OrdinalIgnoreCase)
                                .ThenBy(a => a.FirstName, StringComparer.OrdinalIgnoreCase);
        }
    }
}
