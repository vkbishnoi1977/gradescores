﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace grade_scores.BusinessLogic
{
    public sealed class BasicFileHandlingStrategy : IFileHandlingStrategy
    {
        public List<GradeData> ParseFile(string filePath)
        {

            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentNullException("No file name specified.");
            }

            if (!File.Exists(filePath))
            {
                //Throw another exception
                throw new ArgumentException($"File {filePath} does not exist.");
            }

            var parsedData = new List<GradeData>();
            using (StreamReader reader = new StreamReader(filePath))
            {
                while (!reader.EndOfStream)
                {
                    var data = reader.ReadLine();
                    var dataArray = data.Split(',');

                    if (dataArray.Length < 3)
                    {
                        throw new ArgumentException("Invalid file data.");
                    }

                    int score;

                    if (!int.TryParse(dataArray[dataArray.Length - 1], out score))
                    {
                        throw new ArgumentException("Invalid file data.");
                    }

                    parsedData.Add(new GradeData
                    {
                        Grade = score,
                        LastName = dataArray[0].Trim(),
                        FirstName = dataArray[1].Trim()
                    });
                }
            }

            return parsedData;
        }

        public void WriteFile(string filePath, IEnumerable<GradeData> dataCollection)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentException("Invalid file path.");
            }

            if (dataCollection == null || dataCollection.Count() == 0)
            {
                throw new ArgumentException("There is no data to write.");
            }

            string dataToWrite;
            try
            {
                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    foreach (var data in dataCollection)
                    {
                        dataToWrite = data.ToString();
                        writer.WriteLine(dataToWrite);
                        Console.WriteLine(dataToWrite);
                    }
                }
            }
            catch (NotSupportedException)
            {
                throw new ArgumentException("Invalid file path.");
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Invalid file path.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new ArgumentException("Invalid file path.");
            }
            catch (PathTooLongException)
            {
                throw new ArgumentException("Invalid file path.");
            }
        }
    }
}
