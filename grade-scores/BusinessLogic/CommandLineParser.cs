﻿using System;
using System.IO;

namespace grade_scores.BusinessLogic
{
    public sealed class CommandLineParser
    {
        public string ParseFileName(string[] args)
        {
            if (args == null || args.Length == 0)
                throw new ArgumentNullException("Input file not specified.");

            if (args.Length > 1)
                throw new ArgumentException("More than one command line arguments provided.");

            //Parse the input parameters
            if (!File.Exists(args[0]))
            {
                //Throw another exception
                throw new ArgumentException($"File {args[0]} does not exist.");
            }

            return args[0];
        }
    }
}
