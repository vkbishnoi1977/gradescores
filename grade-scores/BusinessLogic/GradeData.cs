﻿namespace grade_scores.BusinessLogic
{
    public class GradeData
    {
        public int Grade { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public override string ToString()
        {
            return $"{LastName}, {FirstName}, {Grade}";
        }
    }
}
