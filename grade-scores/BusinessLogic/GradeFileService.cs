﻿using System;
using System.Collections.Generic;

namespace grade_scores.BusinessLogic
{
    public sealed class GradeFileService
    {
        IFileHandlingStrategy _fileHandlingStrategy;
        IDataSortingStrategy _dataSortingStrategy;

        public GradeFileService(IFileHandlingStrategy fileParsingStrategy,
                                IDataSortingStrategy dataSortingStrategy)
        {
            if (fileParsingStrategy == null)
            {
                throw new ArgumentNullException($"Null argument passed: {nameof(fileParsingStrategy)}");
            }

            if (dataSortingStrategy == null)
            {
                throw new ArgumentNullException($"Null argument passed: {nameof(dataSortingStrategy)}");
            }

            _fileHandlingStrategy = fileParsingStrategy;
            _dataSortingStrategy = dataSortingStrategy;
        }
        public List<GradeData> ParseFile(string filePath)
        {
            return _fileHandlingStrategy.ParseFile(filePath);
        }

        public void CreateNewFileWithSortedData(List<GradeData> parsedData, string filePath)
        {
            var sortedData = _dataSortingStrategy.SortData(parsedData);

            _fileHandlingStrategy.WriteFile(filePath, sortedData);
        }
    }
}
