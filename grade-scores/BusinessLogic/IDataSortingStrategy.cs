﻿using System.Collections.Generic;

namespace grade_scores.BusinessLogic
{
    public interface IDataSortingStrategy
    {
        IEnumerable<GradeData> SortData(IEnumerable<GradeData> parsedData);
    }
}
