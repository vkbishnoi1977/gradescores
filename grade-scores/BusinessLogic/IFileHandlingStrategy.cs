﻿using System.Collections.Generic;

namespace grade_scores.BusinessLogic
{
    public interface IFileHandlingStrategy
    {
        List<GradeData> ParseFile(string filePath);
        void WriteFile(string filePath, IEnumerable<GradeData> data);
    }
}
