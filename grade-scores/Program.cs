﻿using grade_scores.BusinessLogic;
using System;
using System.IO;

namespace grade_scores
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Parse the command line argument first
                var cmdLineParser = new CommandLineParser();                
                var inputFileName = cmdLineParser.ParseFileName(args);

                //Create the file path to create new file with sorted data
                var gradedFileName = $"{Path.GetFileNameWithoutExtension(inputFileName)}-graded.txt";
                var gradedFileNameWithPath = $"{Path.GetDirectoryName(inputFileName)}\\{gradedFileName}";

                //Parse contents of the file
                var fileParser = new GradeFileService(new BasicFileHandlingStrategy(), new BasicDataSortingStrategy());
                var parsedData = fileParser.ParseFile(inputFileName);

                //Sort the contents and write into a new file
                fileParser.CreateNewFileWithSortedData(parsedData, gradedFileNameWithPath);

                Console.WriteLine($"Finished: created {gradedFileName}");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.Read();
            }

        }
    }
}
